package test.qa.functional;

import com.taite.Listener.TestListener;
import com.taite.Util.WebDriverUtil;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.unitils.core.util.PropertiesReader;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;


import java.io.FileInputStream;
import java.io.InputStream.*;

import org.unitils.core.util.PropertiesReader;
import test.qa.configuration.*;
import test.qa.configuration.*;
import test.qa.utils.*;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * User: pboatwright
 * Date: 3/20/13
 * Time: 12:46 PM
 * @author pboatwright
 */
@Listeners({TestListener.class})
@ContextConfiguration(locations = {"classpath:qa-context.xml"})
public class BaseFT extends AbstractTestNGSpringContextTests {


    @Autowired
    public test.qa.configuration.Constants constants;

    @Autowired
    protected test.qa.configuration.WebDriverConfig webDriverConfig;
    protected EventFiringWebDriver driver;
    protected FluentWait wait;
    protected WebDriverUtil driverUtil;
    protected NavigationUtil nav;
    protected TestUtil testUtil;
    protected AnalyticsUtil analyticsUtil;
    protected ITestContext iTestContext;
    protected SSHUtil sshUtil;

    protected String className;
    protected String browser;
    private Properties autProps;
    private static Resource autResource;
    public SoftAssertions soft;
    public String resultstat;
    public String suiteName;
    public String params;

    public BaseFT() {

         Logger.getLogger("org.openqa.selenium.remote").setLevel(Level.OFF);
         TimeZone.setDefault(TimeZone.getTimeZone("America/Denver"));

    }

    public String testName;
   @BeforeMethod( alwaysRun = true)
    public void setup(Method method,ITestContext testContext) throws Exception {
        this.testName= method.getName();
        soft = new SoftAssertions();
        try {
            this.autProps = loadPropertyFiles();
        }
        catch (IOException exception) {
            throw new Exception("Failed to load the AUT Properties file");

        }
        this.cleanUp();
        this.initDriver(testContext);
        this.driverUtil = new WebDriverUtil(driver, wait,autProps);
        this.driverUtil.wait(2000);
        this.driver.get(constants.APP_URL);
        this.className=testContext.getAllTestMethods()[0].getTestClass().toString();
        this.className=className.substring(className.lastIndexOf("."),className.lastIndexOf("]")).replace(".","");
        this.browser=webDriverConfig.browser;
        if (this.browser.equals("internet explorer")) {
            if (driverUtil.waitForElementPresent("ie11.moreInformation", 2)) {
                driverUtil.click("ie11.moreInformation");
                driver.navigate().to("javascript:document.getElementById('overridelink').click()");
            }
            if (driverUtil.waitForElementPresent("ie11.moreInformationTEXT", 2)) {
                driverUtil.click("ie11.moreInformation");
                driver.navigate().to("javascript:document.getElementById('overridelink').click()");
            }
            if (driver.getTitle().contains("Certificate error")) {
                driver.get("javascript:document.getElementById('invalidcert_continue').click()");
            }
        }
        else if (this.browser.equals("edge")) {
            driverUtil.waitAndClick("edge.details");
            driverUtil.waitAndClick("edge.continue");

        }
        this.initAppUtils();
     }

    @AfterMethod( alwaysRun = true)
    public void tearDown(ITestResult result) {
        //       System.out.println(result.getName());
        String param = Arrays.toString(result.getParameters()).replace("[","").replace("]","");
        if (result.getStatus() == ITestResult.FAILURE){
            if (param.isEmpty()) {
                resultstat = "Failed";
                params = Arrays.toString(result.getParameters()).replace("[", "").replace("]", "");
            }
            else {
                 resultstat = "Failed";
                params = Arrays.toString(result.getParameters()).replace("[", "").replace("]", "");

            }
        }
        else if (result.getStatus() == ITestResult.SKIP){
            resultstat = "Skipped";
            params = Arrays.toString(result.getParameters()).replace("[", "").replace("]", "");
        }
        else if (result.getStatus() == ITestResult.SUCCESS){
            if (param.isEmpty()) {
                 resultstat = "Passed";
                params = Arrays.toString(result.getParameters()).replace("[", "").replace("]", "");
            }
            else {
                 resultstat = "Passed";
                params = Arrays.toString(result.getParameters()).replace("[", "").replace("]", "");
            }
        }
         driverUtil.wait(1000);
        driver.quit();
        //This allows the running of different browsers when running in local run mode (see weDriverConfig browser var)
        if(constants.LOCAL_RUN.equals("Y"))
            webDriverConfig.browser="";

    }

    private final Properties properties = new Properties();
    private Properties loadPropertyFiles() throws Exception{
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/browser.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/header.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/login.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/nav.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/userMngmnt.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/entlmnts.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/notifications.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/explore.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/model.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/behavior.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/dataUtilities.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/analyticDash.properties"));
        properties.load(PropertiesReader.class
                .getResourceAsStream("/pageLocators/entityTimeline.properties"));
        return properties;
    }


    private void initDriver(ITestContext testContext) throws Exception {

        testContext.setAttribute("appVersionop", constants.APP_VERSION);
        driver = webDriverConfig.initDriver(testContext, constants.APP_ENV, constants.DEFAULT_WAIT );

        // Set implicit wait
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // Set explicit and fluent wait
        wait = new WebDriverWait (driver, constants.DEFAULT_WAIT )
                .withTimeout( constants.DEFAULT_WAIT, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
    }

    private void initAppUtils() {
        this.nav=new NavigationUtil(this.driverUtil,this.constants,this.browser);
        this.testUtil=new TestUtil(this.driverUtil,this.constants,this.nav);
        this.analyticsUtil=new AnalyticsUtil(this.driverUtil,this.constants,this.nav);
        this.sshUtil=new SSHUtil(this.driverUtil,this.constants);
    }

    private void cleanUp() { }


}


