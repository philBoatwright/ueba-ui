package test.qa.functional.acceptance;


import org.testng.annotations.Test;
import test.qa.functional.BaseFT;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * User: pboatwright
 * Date: 10/16/18
 * Time: 8:47 AM
 * @author pboatwright
 */
public class Authentication extends BaseFT {

    @Test(priority=1,groups = {"acceptance","3.2"},enabled = true)
    public void verifyLogin() {
        String testname=this.testName;
        nav.adminLogin();
        assertThat(driverUtil.waitForElementPresent("header.analytics",15)).as("Login Successful").isTrue();
    }

 }
