package test.qa.utils;


import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.taite.Util.WebDriverUtil;
import test.qa.configuration.Constants;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * The main data generator utility for sureview
 * User: pboatwright
 * Date: 8/6/16
 * Time: 9:49 AM
 * @author pboatwright
 */
public class SSHUtil {

    protected WebDriverUtil driverUtil;
    protected Constants constants;

    public SSHUtil(WebDriverUtil driverUtil, Constants constants) {
        this.driverUtil=driverUtil;
        this.constants=constants;
    }

    public void SSHExecuteCommand(String user,String password,String host,String command) {

            int port = 22;

            try {

            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, port);
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");
            System.out.println("Establishing Connection...");
            session.connect();
            System.out.println("Connection established.");
            System.out.println("Creating SFTP Channel.");
            Channel channel=session.openChannel("exec");
            ((ChannelExec)channel).setCommand(command);
            channel.setInputStream(null);
            ((ChannelExec)channel).setErrStream(System.err);
            InputStream in=channel.getInputStream();
            channel.connect();
            byte[] tmp=new byte[1024];
            while(true){
                while(in.available()>0){
                    int i=in.read(tmp, 0, 1024);
                    if(i<0)break;
                    System.out.print(new String(tmp, 0, i));
                }
                if(channel.isClosed()){
                    System.out.println("exit-status: "+channel.getExitStatus());
                    break;
                }
                try{Thread.sleep(1000);}catch(Exception ee){}
            }
            channel.disconnect();
            session.disconnect();
            System.out.println("DONE");
        }
          catch (Exception e) {
        e.printStackTrace();
        }
    }


}
