package test.qa.utils;


import com.taite.Util.WebDriverUtil;
import org.openqa.selenium.WebElement;
import test.qa.configuration.Constants;

import java.util.Collections;
import java.util.List;

/**
 * Test Utillty provides access to functionality acroos the appplication
 * User: pboatwright
 * Date: 11/16/18
 * Time: 9:49 AM
 * @author pboatwright
 */
public class AnalyticsUtil {
    private NavigationUtil nav;
    private WebDriverUtil driverUtil;
    private Constants constants;

    public AnalyticsUtil(WebDriverUtil driverUtil, Constants constants,NavigationUtil nav) {
        this.driverUtil=driverUtil;
        this.constants=constants;
        this.nav=nav;
     }
    public void createFeature() {
        driverUtil.click("dataUtilities.clearEventCache");
        driverUtil.click("dataUtilities.clearAnalyticCache");
        driverUtil.alertAccept();
        driverUtil.click("dataUtilities.computeAnalyticCache");
        driverUtil.wait(5000);
    }
    public void createModel(List<Object> model) {
        final int DEL_ALL = 0;
        final int NAME = 1;
        final int DESCRIPTION = 2;
        final int PRIMARY_ROLE = 3;
        final int SECONDARY_ROLE = 4;
        final int AGG_METHOD = 5;
        final int CALC_OUTLIERS = 6;
        final int SAVED_SEARCH = 7;
        final int RQL = 8;
         List<WebElement> webelements =null;
        if ((boolean)(model.get(DEL_ALL))) {
            List<String> existingModles=driverUtil.getTextByList("model.existingModels",0);
             for (String deleteModel : existingModles) {
                 driverUtil.setToExpectedOptionAndClick("model.selectModel",deleteModel);
                 driverUtil.click("model.deleteModel");
                if (driverUtil.alertPresent()) {
                    driverUtil.alertAccept();
                }
            }
        }
        else {
            List<String> existingModels = driverUtil.getTextByList("model.existingModels", 0);
            if (existingModels.contains(model.get(NAME)))
                return;
        }
        driverUtil.hoverText("model.new");
        driverUtil.click("model.createEvent");
        driverUtil.sendkeys("model.name",(String)(model.get(NAME)));
        driverUtil.sendkeys("model.description",(String)(model.get(DESCRIPTION)));
        driverUtil.selectText("model.primaryRole",(String)(model.get(PRIMARY_ROLE)));
        driverUtil.selectText("model.secondaryRole",(String)(model.get(SECONDARY_ROLE)));
        driverUtil.selectText("model.aggMethod",(String)(model.get(AGG_METHOD)));
        if ((boolean)(model.get(CALC_OUTLIERS)))
            driverUtil.click("model.calcOutliers");
        if (!(model.get(SAVED_SEARCH)).equals(""))
            driverUtil.selectText("model.savedSearches",(String)(model.get(SAVED_SEARCH)));
        if (!(model.get(RQL)).equals(""))
            driverUtil.selectText("model.rql",(String)(model.get(RQL)));
        driverUtil.click("model.save");

    }
    public void createScenario(List<Object> scenario) {
        final int TITLE = 0;
        final int SELECT_END_DATE = 1;
        final int CALENDAR = 2;
        final int TIMEPICKER_OPTION = 3;
        final int INTERVALS = 4;
        final int INTERVAL_SIZE = 5;
        final int INTERVAL_TYPE = 6;
        final int INTERVAL_SPREAD = 7;
        final int FILTER = 8;
        final int MODELS = 9;
        List<String> models = (List<String>) (scenario.get(MODELS));
        driverUtil.hoverText("behavior.showExistingScenario");
        List<String> existingScenarios = driverUtil.getTextByList("behavior.existingScenarios", 0);
        for (String deleteScenario : existingScenarios) {
            if (!deleteScenario.equals("Load Existing Scenario")&& !deleteScenario.equals("No Scenarios exist")){
                driverUtil.hoverText("behavior.showExistingScenario");
                driverUtil.setToExpectedOptionAndClick("behavior.existingScenario", deleteScenario);
                driverUtil.click("behavior.delete");
                if (driverUtil.alertPresent()) {
                    driverUtil.alertAccept();
                }
            }
           }
        driverUtil.wait(3000);
        driverUtil.click("behavior.title");
        driverUtil.sendkeys("behavior.addTitle", (String) scenario.get(TITLE));
        if ((boolean)(scenario.get(SELECT_END_DATE)))
            driverUtil.click("behavior.endDate");
        if (!(scenario.get(CALENDAR)).equals("")) {
            driverUtil.sendMultipleBackSpaces("behavior.calendar", 10);
            driverUtil.sendkeys("behavior.calendar", (String) scenario.get(CALENDAR));
            driverUtil.click("behavior.timePicker");
        }
        if (!((scenario.get(TIMEPICKER_OPTION)).equals("")))
            driverUtil.setToExpectedOptionAndClick("behavior.timePickerOption", (String) (scenario.get(TIMEPICKER_OPTION)));
        if ((boolean)(scenario.get(INTERVALS)))
            driverUtil.click("behavior.intervals");
        if (!((scenario.get(INTERVAL_SIZE)).equals("")))
            driverUtil.sendkeys("behavior.intervalSize", (String) (scenario.get(INTERVAL_SIZE)));
        if (!((scenario.get(INTERVAL_TYPE)).equals("")))
            driverUtil.selectText("behavior.intervalType", (String) (scenario.get(INTERVAL_TYPE)));
        if (!((scenario.get(INTERVAL_SPREAD)).equals("")))
            driverUtil.sendKeysBackSpace("behavior.intervalSpread", (String) (scenario.get(INTERVAL_SPREAD)));
        if ((boolean)(scenario.get(INTERVALS)))
            driverUtil.click("behavior.intervalOpens");
        if (!((scenario.get(FILTER)).equals("")))
            driverUtil.selectText("behavior.filter", (String)(scenario.get(FILTER)));
        if (!models.isEmpty()) {
            for (String model : models) {
                {
                    driverUtil.waitAndClick("behavior.openModels");
                    driverUtil.selectText("behavior.selectExistingModel", model);
                    driverUtil.click("behavior.applyForModels");
                }
            }
        }
        driverUtil.click("behavior.apply");
        driverUtil.click("behavior.save");
    }
    public void clearAndGenCache() {
        nav.goToGearOption("Data Utilities");
        driverUtil.click("dataUtilities.clearEventCache");
        driverUtil.wait(2000);
        driverUtil.click("dataUtilities.clearAnalyticCache");
        driverUtil.alertAccept();
        driverUtil.wait(2000);
        driverUtil.click("dataUtilities.computeAnalyticCache");
        driverUtil.wait(2000);
        checkGenCache();
     }
    public boolean cacheEmpty(){
        nav.goToAnalyticsOption("Analytic Dashboard");
        return driverUtil.isElementPresent("analyticDash.Empty");
    }

    public void checkGenCache() {
        driverUtil.getUrl("https://ui-"+constants.APP_HOST+".ro.internal/job-status");
        int i = 0;
        String status = "";
        while (i < 30) {
            status = driverUtil.getText("computePage.computeStatus");
            System.out.println("Status is: " + status);
            driverUtil.wait(1000);
            switch (status) {
                case "success":
                    return;
                case "error":
                    driverUtil.wait(5000);
                    clearAndGenCache();
                    break;
            }
            i++;
        }
    }
}




