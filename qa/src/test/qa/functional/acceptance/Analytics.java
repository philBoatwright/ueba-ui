package test.qa.functional.acceptance;


import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import test.qa.functional.BaseFT;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * User: pboatwright
 * Date: 10/16/18
 * Time: 8:47 AM
 * @author pboatwright
 */
public class Analytics extends BaseFT {
    @Test(dataProvider = "buildAnalyticObjects")
    public void verifyAnalyticsCache(List<Object> models, List<Object> scenario,List<String> modelsForScenario) {
//        if (constants.APP_URL.contains("master.ro")) {
//            System.out.println("This test cannot be run on the master host");
//            assertThat(constants.APP_URL.contains("master.ro")).isFalse();
//            return;
//        }
//
        String Scenario=(String)scenario.get(0);
        nav.adminLogin();
        final int NAME;
        nav.goToGearOption("Models");
        for (Object model : models) {
            analyticsUtil.createModel((List<Object>)model);
        }
        nav.goToGearOption("Behaviors");
        analyticsUtil.createScenario(scenario);
        analyticsUtil.clearAndGenCache();
        nav.goToAnalyticsOption("Analytic Dashboard");
        driverUtil.setToExpectedOptionAndClick("analyticDash.entity","Email Scenario");
        soft.assertThat(driverUtil.setToExpectedOptionAndWaitUntilVisible("entityTimeline.Scenario",Scenario)).isTrue();
        for (String model : modelsForScenario) {
            soft.assertThat(driverUtil.setToExpectedOptionAndWaitUntilVisible("entityTimeline.Model",model)).isTrue();
        }
        soft.assertThat(driverUtil.setToExpectedOptionAndWaitUntilVisible("entityTimeline.Model","Email1")).isTrue();
        soft.assertThat(driverUtil.setToExpectedOptionAndWaitUntilVisible("entityTimeline.Model","Email2")).isTrue();
        soft.assertAll();

    }
    @DataProvider(name = "buildAnalyticObjects")
    private Object[][] buildAnalyticObjects() {
        return new Object[][]{
           { getModels(), getScenario(),getModelsForScenario()},
        };
    }

    private List<Object> getModels() {
            List<Object> models= new ArrayList<>();
            models.add(getEmailModel1());
            models.add(getEmailModel2());
         return models;
    }
    private List<Object> getEmailModel1() {
        List<Object> emailModel= new ArrayList<>();
        //delete all models
        emailModel.add(false);
        //name
        emailModel.add("Email1");
        //Description
        emailModel.add("Automation Created Email1 Model");
        //Primary Role
        emailModel.add("Sender");
        //Secondary Role
        emailModel.add("Recipients");
        //Aggregation Method
        emailModel.add("Event Count");
        //Calculate Outliers:
        emailModel.add(false);
        //Saved Search
        emailModel.add("");
        //Add RQL Statement
        emailModel.add("");
        return emailModel;
    }
    private List<Object> getEmailModel2() {
        List<Object> model= new ArrayList<>();
        //Delete Models
        model.add(false);
        //name
        model.add("Email2");
        //Description
        model.add("Automation Created Email2 Model");
        //Primary Role
        model.add("Sender");
        //Secondary Role
        model.add("Recipients");
        //Aggregation Method
        model.add("Event Count");
        //Calculate Outliers:
        model.add(false);
        //Saved Search
        model.add("");
        //Add RQL Statement
        model.add("");
        return model;
    }
    private List<String> getModelsForScenario() {
        List<String> modelsForScenario= new ArrayList<>();
            modelsForScenario.add("Email1");
            modelsForScenario.add("Email2");
        return modelsForScenario;
    }
    private List<Object> getScenario() {
        List<Object> scenario= new ArrayList<>();
        //Title
        scenario.add("Email Scenario");
        //End Date: Checkbox
        scenario.add(true);
        //Date: Calendar /w field
        scenario.add("");
        //TimePicker Option
        scenario.add("");
        //Timepicker Dropdown
        scenario.add(false);
        //Interval Size Text
        scenario.add("");
        //Interval Type Dropdown
        scenario.add("");
        //Interval Spread
        scenario.add("");
        //Entity Filter Dropdown:
        scenario.add("");
        //Models
        scenario.add(getModelsForScenario());
        return scenario;
    }

}
