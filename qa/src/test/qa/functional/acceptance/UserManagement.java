package test.qa.functional.acceptance;


import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import test.qa.functional.BaseFT;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * User: pboatwright
 * Date: 10/16/18
 * Time: 8:47 AM
 * @author pboatwright
 */
public class UserManagement extends BaseFT {

    @Test(groups = {"none"}, dataProvider = "sharedSearchesScenarios", enabled = true)
    public void verfiyModelerRoleAllowsSharedSearches(String userName, String role, String giveAccess) {
        //RPP9551:SharedSearches  Access
        //As a user with the Modeler role, I can access 'Shared Searches' from the gear icon's drop down.
        //As a user without the Modeler role, I cannot access 'Shared Searches' from the gear icon's drop down.
        nav.adminLogin();
        testUtil.changeUserRole(userName,role,giveAccess);
        driverUtil.setToExpectedOption("nav.gear.option","Shared Searches",false);
        driverUtil.wait(5000);
        driverUtil.click("nav.gear");
        boolean sharedSearchesExists=driverUtil.waitForElementPresent("nav.gear.option",1);
        if (Boolean.parseBoolean(giveAccess))
            assertThat(sharedSearchesExists).as("Could access the Shared Searches option").isTrue();
        else
            assertThat(sharedSearchesExists).as("Could access the Shared Searches option").isFalse();

    }
    @DataProvider(name = "sharedSearchesScenarios")
    private Object[][] sharedSearches() {
        return new Object[][]{
                {"Red Owl", "modeler", "true"},
                {"Red Owl", "modeler", "false"}
        };
    }

    @Test(groups = {"none"}, dataProvider = "EventPermission", enabled = true)
    public void verifyCorrectMessageForNoPermissionToSeeEvent(String userName, String email, String newPassword,String retypePassword,List<String> roles, String entitlement,String entitleRQL) {
        //  RPP10982: NoPermissionMessageToViewEvent
        //        QA - As a user I need to see the "You do not have permission to view this event" when I click an event that I don't have permission to view.
//        Steps to reproduce :
//        Select or create a user (User X) and select an entitlement for that user.
//        Search through the events for the Entity="Cristy Graff", add a note tagging User X.
//                Log in as User X and confirm that the notification for the event NOTE tag appears in the Notification Modal and on the Notifications Page.
//        Click on the notification on the Notifications Page.
//        The EV should open and display a message: 'You do not have permission to view this event'.
        String note = "  Now you cannot see the entitlement. You do not have permission.";
        nav.adminLogin();
        testUtil.createEntitlement(entitlement,entitleRQL);
        testUtil.createEditUser(userName,email,newPassword,retypePassword,roles,entitlement);
        testUtil.addNote(userName,note);
        nav.logout();
        driverUtil.wait(2000);
        nav.login(email, newPassword);
        nav.goToNotification();
        driverUtil.click("header.notifications.seeAll");
        driverUtil.setToExpectedOptionAndClick("notifications.message",note);
        assertThat(driverUtil.waitForElementPresent("notification.ZeroEvents",1)).as("O Events").isTrue();

    }
    @DataProvider(name = "EventPermission")
    private Object[][] eventPermission() {
        return new Object[][]{
                {"Wilma Flintstone", "wilma@bedrock.com", "Password1!","Password1!",getAdminRole(),"email","NOT entity=\"email\""},
        };
    }
    private List<String> getAdminRole() {
        List<String> adminRole = new ArrayList<String>();
        adminRole.add("user");
        adminRole.add("analyst");
        adminRole.add("admin");
        adminRole.add("modeler");
        adminRole.add("developer");

        return adminRole;
    }

}
