package test.qa.utils;

import com.taite.Util.WebDriverUtil;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import test.qa.configuration.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The main navigation for sureview
 * User: pboatwright
 * Date: 3/6/13
 * Time: 9:49 AM
 * @author pboatwright
 */
public class NavigationUtil {

    protected EventFiringWebDriver driver;
    protected WebDriverUtil driverUtil;
    protected FluentWait wait;
    protected WebDriverWait spinnerWait;
    protected Constants constants;
    protected ITestContext iTestContext;
    protected  String browser;

    public NavigationUtil(WebDriverUtil driverUtil, Constants constants, String browser) {

        this.driverUtil=driverUtil;
        this.constants=constants;
        this.browser=browser;

    }
    public void adminLogin() {
        String user=constants.APP_ADMIN_USERNAME;
        String pwd=constants.APP_ADMIN_PASSWORD;

        login(user,pwd);

    }

    public void login(String user, String pwd) {
        this.driverUtil.waitForElementPresent("login.submit", 2);
        this.driverUtil.sendKeys("login.email", user);
        this.driverUtil.wait(1000);
        this.driverUtil.sendKeys("login.password", pwd);
        this.driverUtil.waitUntilVisible("login.submit");
        this.driverUtil.click("login.submit");
        if (this.driverUtil.isElementPresent("login.agree"))
            this.driverUtil.click("login.agree");
        this.driverUtil.waitForElementVisible("header.analytics",2);
    }
    public void logout() {
        driverUtil.wait(2000);
        driverUtil.click("nav.gear");
        driverUtil.click("nav.gear.logout");

    }

    public void goToGearOption(String option) {;
        driverUtil.wait(1000);
        driverUtil.click("nav.gear");
        driverUtil.setToExpectedOptionAndClick("nav.gear.option",option);
//        driverUtil.setToExpectedOption("nav.gear.option",option.toLowerCase(),false);
////       String fontWeightOfOptionToSelect=driverUtil.getCSSPropertyValue("NavDropdown-link-active","font-weight","nav.gear.option");
//        if (driverUtil.isElementPresent("nav.gear.option"))
       }
    public void goToAnalyticsOption(String option) {
        driverUtil.wait(5000);
        driverUtil.click("nav.analytics");
        driverUtil.setToExpectedOptionAndClick("nav.analytics.option",option);

    }
    public void goToNotification(){
        driverUtil.click("nav.notifications");

    }


}
