package test.qa.configuration;

import org.springframework.beans.factory.annotation.Value;

import javax.inject.Named;

/**
 * Constants pulled from the property file
 * User: pboatwright
 * Date: 3/6/13
 * Time: 9:48 AM
 */
@Named
public class Constants {
    public boolean DB_RAN_ONCE=false;

    @Value("${app.localRun}")
    public String LOCAL_RUN;

    @Value("${app.defaultBrowser}")
    public String DEFAULT_BROWSER;

    @Value("${app.gridHub}")
    public String GRID_HUB;

    //App Settings
    @Value("${app.admin.username}")
    public String APP_ADMIN_USERNAME;

    @Value("${app.admin.password}")
    public String APP_ADMIN_PASSWORD;

    @Value("${app.env}")
    public String APP_ENV;

    @Value("${app.host}")
    public String APP_HOST;

    @Value("${app.host.port}")
    public Integer APP_HOST_PORT;

    @Value("${app.version}")
    public String APP_VERSION;

    @Value("${app.url}")
    public String APP_URL;

    @Value("${app.default.wait}")
    public Long DEFAULT_WAIT;
    @Value("${ssh.URL}")
    public String SSH_URL;

    @Value("${ssh.user}")
    public String SSH_USER;

    @Value("${ssh.password}")
    public String SSH_PASSWORD;

    @Value("${ssh.CCEntity}")
    public String SSH_CCENTITY;

    @Value("${ssh.SystemAdmin}")
    public String SSH_SYSTEM_ADMIN;
    

 }
