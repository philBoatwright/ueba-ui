package test.qa.configuration;

import com.taite.Configuration.TaiteWebDriverConfig;
import com.google.inject.Inject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmentable;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.testng.ITestContext;

import java.net.MalformedURLException;

/**
 * User: pboatwright
 * Date: 3/28/13
 * Time: 4:43 PM
 */
@Augmentable
@Component
public class WebDriverConfig extends TaiteWebDriverConfig {

    @Autowired
    protected Constants constants;
    protected EventFiringWebDriver driver;
    public String browser = ""; //leave this blank
    protected String browserVersion = "";
    protected String platform = "VISTA";


    public EventFiringWebDriver initDriver(ITestContext testContext, String env, Long defaultWait) throws MalformedURLException, InterruptedException {
         if (testContext.getCurrentXmlTest().getParameter("browser")==null) {
            this.browser=constants.DEFAULT_BROWSER;
            testContext.setAttribute("browser",this.browser);
            testContext.setAttribute("platform", platform);
            testContext.setAttribute("browserVersion", browserVersion);


         }
        else {
            this.browser = testContext.getCurrentXmlTest().getParameter("browser");
            platform = testContext.getCurrentXmlTest().getParameter("platform");
            browserVersion = testContext.getCurrentXmlTest().getParameter("browserVersion");

        }

        if(constants.LOCAL_RUN.equals("Y")) {
            initLocalDriver(testContext, defaultWait);
        }
        else
             try {
                initRemoteDriver(testContext, defaultWait);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        return this.driver;

    }

    public void initLocalDriver(ITestContext testContext, Long defaultWait) throws MalformedURLException, InterruptedException {

        WebDriver driver = augmentedWebDriver(browser, platform, browserVersion, testContext);
        this.driver = eventFiringRemoteWebDriver(driver, defaultWait);

    }

    public void initRemoteDriver(ITestContext testContext, Long defaultWait) throws MalformedURLException, InterruptedException {

        WebDriver webDriver = augmentedRemoteWebDriver(browser, "", "WINDOWS", testContext,constants.GRID_HUB);
        this.driver = eventFiringRemoteWebDriver(webDriver, defaultWait);

    }

}