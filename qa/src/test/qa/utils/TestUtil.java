package test.qa.utils;


import com.taite.Util.WebDriverUtil;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import test.qa.configuration.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Test Utillty provides access to functionality acroos the appplication
 * User: pboatwright
 * Date: 11/16/18
 * Time: 9:49 AM
 * @author pboatwright
 */
public class TestUtil {
    protected NavigationUtil nav;
    protected WebDriverUtil driverUtil;
    protected Constants constants;

    public TestUtil(WebDriverUtil driverUtil, Constants constants,NavigationUtil nav) {
        this.driverUtil=driverUtil;
        this.constants=constants;
        this.nav=nav;
     }
    public void changeUserRole(String userName,String role, String status ) {
        nav.goToGearOption("User Management");
        driverUtil.wait(2000);
        driverUtil.waitAndClick("userMngmnt.users.edit");
        boolean givePermission=Boolean.parseBoolean(status);
        driverUtil.setToExpectedOption("userMngmnt.role.IsChecked",role,false);
        driverUtil.setToExpectedOption("userMngmnt.role.NotChecked",role,false);
        if (driverUtil.isElementPresent("userMngmnt.role.IsChecked") & !givePermission ) {
            driverUtil.wait(2000);
            driverUtil.setToExpectedOptionAndClick("userMngmnt.role",role);
          }
        else if (driverUtil.isElementPresent("userMngmnt.role.NotChecked") & givePermission) {
            driverUtil.wait(2000);
            driverUtil.click("userMngmnt.role." + role);
        }
        driverUtil.click("userMngmnt.role.save");

    }
    public void createEditUser(String name,String email, String newPassword, String retypePassword,List<String> desiredRoles,String entitlement ) {
        nav.goToGearOption("User Management");
        driverUtil.wait(2000);
        if (userExists(name))
            driverUtil.setToExpectedOptionAndClick("userMngmnt.users.edit",name);
        else {
            driverUtil.waitAndClick("userMngmnt.users.add");
            driverUtil.sendkeys("userMngmnt.createUser.name", name);
            driverUtil.sendkeys("userMngmnt.createUser.email", email);
            driverUtil.sendkeys("userMngmnt.createUser.newPassword", newPassword);
            driverUtil.sendkeys("userMngmnt.createUser.retypePassword", retypePassword);
        }
        List<String> allRolesChecked=driverUtil.getTextByList("userMngmnt.role.rolesChecked",0);
        for (String role : allRolesChecked ) {
               driverUtil.setToExpectedOptionAndClick("userMngmnt.role.IsChecked", role.toLowerCase());
        }
        for (String desiredRole : desiredRoles ){
            driverUtil.setToExpectedOptionAndClick("userMngmnt.role",desiredRole);
        }
        driverUtil.setToExpectedOptionAndClick("userMngmnt.entitlement",entitlement);
        driverUtil.click("userMngmnt.role.save");

    }
    public void createEntitlement(String name,String rql) {
        nav.goToGearOption("Entitlements");
        if (entitlementExists(name)){
            return;
        }
        driverUtil.wait(2000);
        driverUtil.waitAndClick("entlmnts.add");
        driverUtil.sendkeys("entlmnts.name",name);
        driverUtil.sendkeys("entlmnts.summary",name);
        driverUtil.sendkeys("entlmnts.rql",rql);
        driverUtil.click("entlmnts.submit");


    }
    public void addNote(String userName,String note) {
        nav.goToAnalyticsOption("Explore");
        driverUtil.click("explore.note");
        driverUtil.wait(2000);
       if (driverUtil.setToExpectedOptionAndWaitUntilVisible("explore.email.noteSent",note)) {
           driverUtil.click("explore.note.close");
           return;
       }
        driverUtil.sendkeys("explore.notification.note","@"+userName);
        if (driverUtil.isElementPresent("explore.suggestion"))
            driverUtil.click("explore.suggestion");
        driverUtil.sendkeys("explore.notification.note"," "+note);
        driverUtil.click("explore.submit");
        driverUtil.click("explore.note.close");



    }

    public boolean userExists(String userName) {
        List<String> currentUsers=driverUtil.getTextByList("userMngmnt.users.all",true,0);
        return currentUsers.contains(userName.toUpperCase());
    }

    public boolean entitlementExists(String entName) {
        List<String> allCells=driverUtil.getTextByList("entlmnts.grid.allCells",true,0);
        return allCells.contains(entName.toUpperCase());


    }

}

